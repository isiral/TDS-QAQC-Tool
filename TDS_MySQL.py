import cx_Oracle
import os
import getpass
import re

import datetime


class TDS_MySQL:

    cnx = None
    WorkMode="PlayGround"
    def __init__(self,DB_WorkMode="PlayGround",Site=False,Equipment=False):

        self.user=getpass.getuser()
        self.PCuser=getpass.getuser()
        self.Site = Site
        self.Equipment = Equipment
        self.WorkMode=DB_WorkMode
        if self.WorkMode is "PlayGround":
            print "Runing on PlayGround mode"
        elif self.WorkMode is "Production":
            print "Runing on Production mode"
        else:
            print "The database type",self.WorkMode,"is invalid, exiting."
            exit()


        password = getpass.getpass(prompt="Please Enter the Database Password:")

        
            
        if self.WorkMode is "PlayGround":
            #dns_tns = cx_Oracle.makedsn("db-d0002.cern.ch","10654","int8r.cern.ch")
            #dns_tns = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST =db-d0002.cern.ch)(PORT = 10654)) (LOAD_BALANCE = on)(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = int8r.cern.ch) ) )" 
            dns_tns = "(DESCRIPTION= (ADDRESS= (PROTOCOL=TCP) (HOST=int8r-s.cern.ch) (PORT=10121) ) (LOAD_BALANCE=on)(ENABLE=BROKEN)(CONNECT_DATA=(SERVER=DEDICATED) (SERVICE_NAME=int8r.cern.ch) ) )"           
                 
            self.cnx = cx_Oracle.connect("ATLAS_MUON_NSW_ELEC_QAQC_W",password,dns_tns)


        elif self.WorkMode is "Production":
            dns_tns = "(DESCRIPTION= (ADDRESS= (PROTOCOL=TCP) (HOST=atlr-s.cern.ch) (PORT=10121) ) (LOAD_BALANCE=on)(ENABLE=BROKEN)(CONNECT_DATA=(SERVER=DEDICATED) (SERVICE_NAME=atlas_muon_nsw.cern.ch) ) )"           
            self.cnx = cx_Oracle.connect("ATLAS_MUON_NSW_ELEC_QAQC_W",password,dns_tns)

            # self.cursor=self.cnx.cursor()
        # except:
        #     print "Database cannot be opened"
        #     raise Exception('Database Problem')            

        
        self.TableDict={}
        self.TypeDict={}
        self.Create_Table_Dict()

    
## This a basic sort function used by some tools.
    def SortInputs(self,List,Inputs):
        List = [ tmpList.upper() for tmpList in List ]
        Inputs = [ tmpList.upper() for tmpList in Inputs ]
        if len(Inputs)==0: return []

        Output=[ None for i in range(0,len(List)) ]
        for index in range(0,len(List)):
            for Input in Inputs:
                if not "=" in Input: continue
                Input=Input.split('=')
                if List[index]==Input[0]:
                    if "FILE" in Input[0] or "IMAGE" in Input[0]:
                        try:
                            Input[1]=open(Input[1]).read()
                        except:
                            print "File/Image Failed to Open"
                            return False;
                    Output[index]=Input[1]

#        print Output
        return Output



## This runs command in SQL with commit
    def RunCommand(self,Command,InputValues=[],Files=[],ReturnVal=False):
        Command = Command.upper()


        try:
            cursor=self.cnx.cursor()
        except:
            print "Failed to create cursor"
            return False


        # try:
        if len(InputValues)>0 or len(Files)==2:


            if len(Files)==2:
                binary_var = cursor.var(cx_Oracle.BLOB)
                binary_var.setvalue(0, Files[1])
                
            
                InputValues = list(InputValues)
                InputValues[Files[0]-1] = binary_var
                InputValues = tuple(InputValues)
            
            if ReturnVal:
                ReturnVal = cursor.var(cx_Oracle.STRING)
                InputValues+=tuple([ReturnVal])
                #print Command, InputValues
                cursor.execute(Command,InputValues)
                ReturnVal=ReturnVal.getvalue();
            else:
                
                cursor.execute(Command,InputValues)
        else:
            cursor.execute(Command)

        # except:
        #     print "Failed to Run Show/Select Command \n",Command,"InputValues",InputValues
        #     return False
            
        self.cnx.commit()
        cursor.close()
        return ReturnVal

## THis runs commands without commit "To Show"
    def RunShow(self,Command,InputValues=[],DESC=False):
        Command = Command.upper()


        try:
            cursor=self.cnx.cursor()
        except:
            print "Failed to create cursor"
            return False



        try:
            if len(InputValues)>0:
                cursor.execute(Command,InputValues)
            else:
                cursor.execute(Command)
                
            if  DESC :
                Results = cursor.description
                #print Results
            else:
                Results = cursor.fetchall()
            cursor.close()
       
        except:
             print "Failed to Run Show/Select Command \n",Command,"InputValues",InputValues
             return False
        

        
        return Results



## Adds Item to a Table
    def AddItem(self,NameTable,InputValues,Channel=None):

        Time=None
        if len(InputValues)>3:
            Time=InputValues[3]


        InputValues=InputValues[0:3]

        if not self.Check_Table(NameTable):
            print "Error!",NameTable, "is not found. Skipping..."
            return
        InputValues[2]=InputValues[2].upper()
        if not InputValues[2]=="T" and not InputValues[2]=="F":
            print "Error! Is Valid Tag must be T/F, you have given:",InputValues[1]
            return
        

        try:
            if Time:
                Time=Time.split("-")
                Time=datetime.date(int(Time[0]),int(Time[1]),int(Time[2]))
            else:
                Time=datetime.date.today()
        except:
            print "Invalid Time Stamp used:",Time,"Please use the format Year-Month-Date"
            return



        #This line checks the user given ID, if it's not EQ_ID it will convert to suitable EQ_ID, if no candidates found it will complain
        tmpInput=InputValues[0]
        InputValues[0]=self.FindEQID(tmpInput)
        if InputValues[0] is False:
            print "Invalid Chip Name,Number,ID",tmpInput
            return






        InputCheck = self.CheckInputs(NameTable,InputValues[1],Channel)
        # print InputCheck
        ### Input Check ###
        if InputCheck==0:
            print "Error!, Invalid input:",InputValues[1],"for",NameTable
        ### Normal Variable ###
        elif InputCheck==1:
            try: 
                user= self.user
                PCuser= self.PCuser
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS (CONTEXTNAME,EQENTRYID,MEASVALUE,ISVALIDFLAG,SHIFTER,USERCR,USERED,MEASTIME) VALUES( :1, :2, :3, :4, :5, :6 , :7, :8)"
                if Channel: 
                    Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS (CONTEXTNAME,EQENTRYID,MEASVALUE,ISVALIDFLAG,SHIFTER,USERCR,USERED,MEASTIME,CHANNEL) VALUES( :1, :2, :3, :4, :5, :6 , :7, :8, :9)"
                InputValues+=[user,PCuser,PCuser]
                InputValues=[NameTable]+InputValues+[Time]
                if Channel:
                    InputValues+=[Channel]
                self.RunCommand(Command,tuple(InputValues))                    
            except:
                print "Error! Failed Adding. Command:", Command," ARGS",InputValues
                return
        ### Adding Files ###
        elif InputCheck==2:
            try:
                tmpFile=open(InputValues[1]).read()
                if InputValues[1].count(".")>0:
                    FileType=InputValues[1].split(".")[-1][0:3]
                else:
                    FileType=""
            except:
                print "File",InputValues[1],"Failed to Open"
                return;
            try:
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS (CONTEXTNAME,EQENTRYID,ISVALIDFLAG,SHIFTER,USERCR,USERED,MEASTIME) VALUES( :1, :2, :3, :4, :5,:6,:7) returning MEASSITEHASH into :8"
                if Channel: 
                    Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS (CONTEXTNAME,EQENTRYID,ISVALIDFLAG,SHIFTER,USERCR,USERED,MEASTIME,CHANNEL) VALUES( :1, :2, :3, :4, :5,:6,:7,:8) returning MEASSITEHASH into :9"
                user= self.user
                PCuser= self.PCuser
                CMDIn=InputValues[0:1]+InputValues[2:]+[user,PCuser,PCuser]
                CMDIn=[NameTable]+CMDIn+[Time]
                if Channel:
                    CMDIn+=[Channel]
                idCentralMeas = self.RunCommand(Command,tuple(CMDIn),[],True)                    
                
            except:
                    print "Error! Failed Adding. Command:", Command," ARGS",CMDIn
                    return

            try:
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS (TYPEOFDOC, FILEFORMAT, DOCNAME, ISVALIDFLAG, USERCR, USERED,DOCCONTENT) VALUES (:1,:2,:3,:4,:5,:6,:7) returning MEASDOCHASH into :8"
                CMDIn=InputValues[1:]+[user,user]
                CMDIn=[NameTable,FileType]+CMDIn+[""]
                CMDIn[2]=CMDIn[2].split("/")[-1]

                idMeasDoc = self.RunCommand(Command,tuple(CMDIn),[7,tmpFile],True)      
                
            except:
                print "Error! Failed Adding. Command:", Command," ARGS ",CMDIn
                return

            try:
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK (MEASSITEHASH,DOCHASH,ISVALIDFLAG,USERCR,USERED) VALUES( :1, :2, :3, :4, :5)"
                PCuser= self.PCuser
                CMDIn=InputValues[2:]+[PCuser,PCuser]
                CMDIn=[idCentralMeas,idMeasDoc]+CMDIn
                idCentralMeas = self.RunCommand(Command,tuple(CMDIn))                    

            except:
                print "Error! Failed Adding. Command:", Command," ARGS",CMDIn
                return


        elif InputCheck==3:
            tmpFile=InputValues[1]
            FileType="ARRAY"
        
            try:
                
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS (CONTEXTNAME,EQENTRYID,ISVALIDFLAG,SHIFTER,USERCR,USERED,MEASTIME) VALUES( :1, :2, :3, :4, :5,:6, :7) returning MEASSITEHASH into :8"
                if Channel:
                    Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS (CONTEXTNAME,EQENTRYID,ISVALIDFLAG,SHIFTER,USERCR,USERED,CHANNEL,MEASTIME) VALUES( :1, :2, :3, :4, :5,:6,:7,:8) returning MEASSITEHASH into :9"
                user= self.user
                PCuser= self.PCuser
                CMDIn=InputValues[0:1]+InputValues[2:]+[user,PCuser,PCuser]
                CMDIn=[NameTable]+CMDIn+[Time]
                if Channel:
                    CMDIn+=[Channel]
                idCentralMeas = self.RunCommand(Command,tuple(CMDIn),[],True)                    
                
            except:
                    print "Error! Failed Adding. Command:", Command," ARGS",CMDIn
                    return

            try:
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS (TYPEOFDOC, FILEFORMAT, ISVALIDFLAG, USERCR, USERED,DOCCONTENT) VALUES (:1,:2,:3,:4,:5,:6) returning MEASDOCHASH into :7"
                CMDIn=InputValues[2:]+[user,user]
                CMDIn=[NameTable,FileType]+CMDIn+[""]
                idMeasDoc = self.RunCommand(Command,tuple(CMDIn),[6,tmpFile],True)      
                
            except:
                print "Error! Failed Adding. Command:", Command," ARGS ",CMDIn
                return

            try:
                Command = "INSERT INTO ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK (MEASSITEHASH,DOCHASH,ISVALIDFLAG,USERCR,USERED) VALUES( :1, :2, :3, :4, :5)"
                PCuser= self.PCuser
                user= self.user
                
                CMDIn=InputValues[2:]+[PCuser,PCuser]
                CMDIn=[idCentralMeas,idMeasDoc]+CMDIn
                idCentralMeas = self.RunCommand(Command,tuple(CMDIn))                    

            except:
                print "Error! Failed Adding. Command:", Command," ARGS",CMDIn
                return

        print "Add Process Complete"








## Modifies Item in a Table 
    def ModifyItem(self,NameTable,InputValues):
       
            

        
        if not self.Check_Table(NameTable):
            print "Error!",NameTable, "is not found. Skipping..."
            return
        InputValues[2]=InputValues[2].upper()
        if not InputValues[2]=="T" and not InputValues[2]=="F":
            print "Error! Is Valid Tag must be T/F, you have given:",InputValues[2]
            return
        
        InputCheck = self.CheckInputs(NameTable,InputValues[1])

        if InputCheck==0:
            print "Error!, Invalid input:",InputValues[1],"for",NameTable
        elif InputCheck==1:
            try:
                Command = """UPDATE ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS SET MEASVALUE=:1,ISVALIDFLAG=:2,USERED=:3 WHERE ID_CENTRALMEAS=:4 and CONTEXTNAME=:5"""
                user= self.user
                fTuple=tuple(InputValues[1:]+[user,InputValues[0],NameTable])
                self.RunCommand(Command,fTuple)
            except:
                print "Error! Failed Modifying. Command:", Command," ARGS",fTuple
                return
        elif InputCheck==2:
            try:
                tmpFile=open(InputValues[1]).read()
                if InputValues[1].count(".")>0:
                    FileType=InputValues[1].split(".")[-1][0:3]
                else:
                    FileType=""
            except:
                print "File",InputValues[1],"Failed to Open"
                return;


            try:
                Command = "select ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK.DOCHASH from ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK right join ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS on ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK.MEASSITEHASH=ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS.MEASSITEHASH where ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS.MEASSITEHASH=:1 AND ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS.CONTEXTNAME=:2" 
                fTuple=tuple([InputValues[0],NameTable])
                Results = self.RunShow(Command,fTuple)
                
                if len(Results)<1: 
                    print "Cannot Find the Measurement",fTuple
                    return 

                user= self.user
                
                Command = "UPDATE ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS SET ISVALIDFLAG=:1,USERED=:2,FILEFORMAT=:3,DOCCONTENT=:4 WHERE MEASDOCHASH=:5"
                CMDIn=InputValues[2:3]+[user]
                CMDIn=CMDIn+[FileType,"",Results[0][0]]
                self.RunCommand(Command,tuple(CMDIn),[4,tmpFile])      

 
            except:
                    print "Error! Failed Modify. Command:", Command," ARGS",CMDIn
                    return


            try:
                Command = "UPDATE ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS SET ISVALIDFLAG=:1,USERED=:2 WHERE ID_CENTRALMEAS=:3 and CONTEXTNAME=:4" 
                user= self.user
                fTuple=tuple(InputValues[2:3]+[user,InputValues[0],NameTable])
                self.RunCommand(Command,fTuple)

                Command = "UPDATE ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK SET ISVALIDFLAG=:1 WHERE MEASSITEHASH=:2"
                fTuple2=tuple(InputValues[2:3]+InputValues[0:1])
                self.RunCommand(Command,fTuple2)
            except:
                    print "Error! Failed Modify. Command:", Command," ARGS",fTuple
                    return
        elif InputCheck==3:

            tmpFile=InputValues[1]
            FileType="ARRAY"

            try:
                Command = "select ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK.DOCHASH from ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK right join ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS on ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK.MEASSITEHASH=ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS.MEASSITEHASH where ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS.MEASSITEHASH=:1 AND ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS.CONTEXTNAME=:2" 
                fTuple=tuple([InputValues[0],NameTable])
                Results = self.RunShow(Command,fTuple)
                
                if len(Results)<1: 
                    print "Cannot Find the Measurement",fTuple
                    return 

                user= self.user


                Command = "UPDATE ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS SET ISVALIDFLAG=:1,USERED=:2,FILEFORMAT=:3,DOCCONTENT=:4 WHERE MEASDOCHASH=:5"
                CMDIn=InputValues[2:3]+[user]
                CMDIn=CMDIn+[FileType,"",Results[0][0]]
                self.RunCommand(Command,tuple(CMDIn),[4,tmpFile])      

 
            except:
                    print "Error! Failed Modify. Command:", Command," ARGS",CMDIn
                    return

            try:
                Command = "UPDATE ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS SET ISVALIDFLAG=:1,USERED=:2 WHERE ID_CENTRALMEAS=:3 and CONTEXTNAME=:4" 
                user= self.user
                fTuple=tuple(InputValues[2:3]+[user,InputValues[0],NameTable])
                self.RunCommand(Command,fTuple)

                Command = "UPDATE ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK SET ISVALIDFLAG=:1 WHERE MEASSITEHASH=:2"
                fTuple2=tuple(InputValues[2:3]+InputValues[0:1])
                self.RunCommand(Command,fTuple2)
            except:
                    print "Error! Failed Modify. Command:", Command," ARGS",fTuple
                    return

            
                

## Deletes Items in a Table
    def DeleteItem(self,InputValues):
        Command = "delete from ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS where ID_CENTRALMEAS=:1"
        print InputValues, len(InputValues)
        if len(InputValues)==0: print "No variables given. Skipping..."; return
        if len(InputValues)>1: print "Must be exactly one variable. Skipping..."; return
        if not InputValues[0].isdigit() : print "ID must be integer. Skipping..."; return
        try:
            self.RunCommand(Command,tuple(InputValues))
        except:
            print "Error! Failed Deleting. Command",Command," ARGS",InputValues
## Show Items in a Table




    def DownloadFile(self,NameTable,DocHash,OutFile=False):
        
        InputCheck = self.CheckInputs(NameTable,"","BYPASS")
        if not InputCheck is 2:
            print NameTable,"doesn't store files or doesn't exsist."
            return 
        


        try:
            Command = "SELECT ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS.DOCCONTENT,ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS.DOCNAME FROM ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS LEFT JOIN ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK ON ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCUMENTS.MEASDOCHASH=ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK.DOCHASH WHERE ATLAS_MUON_NSW_ELEC_QAQC.MEASDOCLINK.MEASSITEHASH=:1"
            Results = self.RunShow(Command,tuple([DocHash]))
        except:
            print "Failed to run command",Command,DocHash
            return 


        if len(Results)==0:
            print "File cannot be found, if you think this is a problem please contact an admin"
            return
        elif len(Results)>1:
            print "More then one file has been matched for this criteria, this signifies a huge data integrity problem, please contact an admin!"
            return

        try:
            imageBlob = Results[0][0]
            Name = Results[0][1]

            if OutFile:
                Name=OutFile

            directory="DownloadedFiles/"
            if not os.path.exists(directory):
                os.makedirs(directory)
                print "Directory",directory,"doesn't exist. Creating.."
            Name=directory+Name
            ofile = open(Name,"w")
            ofile.write(imageBlob.read())
            ofile.close()
            print "File written",Name
        except:
            print "Failed to write the file."
            return





    def ShowTable(self,NameTable, ReturnMode=False):
        NameTable = NameTable.upper()
        print NameTable


        

        try:
            Command = "select MEASSITEHASH,EQENTRYID,MEASVALUE, ISVALIDFLAG,CHANNEL, USERCR, USERED, CREATETIME,EDITTIME from ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS WHERE upper(CONTEXTNAME)=upper(:1)"
            Results = self.RunShow(Command,tuple([NameTable]))
            Headers = self.RunShow(Command,tuple([NameTable]),True)
            Headers = [ Head[0] for Head in Headers] 


            print "Table:",NameTable



            row_format=[]
            line=""
            for Head in Headers:
                
                row_format.append("{:>"+str(len(Head))+"}")
                line+="-"*(len(Head)+9)
                print Head,"   |   ",
            print
            print line
            for Line in Results:
                for index in range(0,len(Headers)):
                    print row_format[index].format(str(Line[index])),"   |   ",
                print


            if ReturnMode: 
                return Headers,Results






        except:
            print "Failed to run Show Command"

    def ShowHistory(self,NameTable, ReturnMode=False):
        NameTable = NameTable.upper()
        print NameTable

        try:
            Command = "select CONTEXTNAME,MEASSITEHASH,MEASVALUE, ISVALIDFLAG, USERCR, USERED, CREATETIME,EDITTIME from ATLAS_MUON_NSW_ELEC_QAQC.CENTRALMEAS WHERE EQENTRYID=:1"
            Results = self.RunShow(Command,tuple([NameTable]))
            Headers = self.RunShow(Command,tuple([NameTable]),True)
            Headers = [ Head[0] for Head in Headers] 

            if ReturnMode: 
                return SelCol,Results

            print "Equipment Entry ID:",NameTable

            row_format=[]
            line=""
            for Head in Headers:
                row_format.append("{:>"+str(len(Head))+"}")
                line+="-"*(len(Head)+9)
                print Head,"   |   ",
            print
            print line
            for Line in Results:
                for index in range(0,len(Headers)):
                    print row_format[index].format(str(Line[index])),"   |   ",
                print


        except:
            print "Failed to run Show Command"



            


#### From here on these are functions that modifies tables ########## 



    #Checks if the table/test exists 
    def Check_Table(self,InputValues):

        for NameTable in self.TableDict.keys():
            #print NameTable,self.TableDict[NameTable]
            if InputValues in self.TableDict[NameTable]:
                return 1

        return 0;



#Creates a dictionary with tables and their info        
    def Create_Table_Dict(self):
        if len(self.TableDict)>0: return self.TableDict



        Command = "select COLLECTIONNAME from ATLAS_MUON_NSW_ELEC_QAQC.COLLECTIONS"

        Results = self.RunShow(Command,[],False)


        
        for Type in Results:


            if self.Site and self.Equipment:
                Command = "select CONTEXTS.CONTEXTNAME,ASPECTS.NUMBEROFCHANNELS,CONTEXTS.ALLOWEDDEVHIGH,CONTEXTS.ALLOWEDDEVLOW from ATLAS_MUON_NSW_ELEC_QAQC.CONTEXTS left join ATLAS_MUON_NSW_ELEC_QAQC.ASPECTS ON ASPECTS.ASPECTNAME=CONTEXTS.ASPECTNAME left join ATLAS_MUON_NSW_ELEC_QAQC.COLLCOMPONENTS ON COLLCOMPONENTS.ASPECTNAME=ASPECTS.ASPECTNAME where COLLCOMPONENTS.COLLECTIONNAME like :1 and  CONTEXTS.SITENAME like :2 and ASPECTS.EQTYPEGROUPCODE like :3"
                Type=Type[0:1]+tuple([self.Site,self.Equipment])
            elif self.Equipment:
                Command = "select CONTEXTS.CONTEXTNAME,ASPECTS.NUMBEROFCHANNELS,CONTEXTS.ALLOWEDDEVHIGH,CONTEXTS.ALLOWEDDEVLOW  from ATLAS_MUON_NSW_ELEC_QAQC.CONTEXTS left join ATLAS_MUON_NSW_ELEC_QAQC.ASPECTS ON ASPECTS.ASPECTNAME=CONTEXTS.ASPECTNAME left join ATLAS_MUON_NSW_ELEC_QAQC.COLLCOMPONENTS ON COLLCOMPONENTS.ASPECTNAME=ASPECTS.ASPECTNAME where COLLCOMPONENTS.COLLECTIONNAME like :1 and ASPECTS.EQTYPEGROUPCODE like :2"
                Type=Type[0:1]+tuple([self.Equipment])
            elif self.Site:
                Command = "select CONTEXTS.CONTEXTNAME,ASPECTS.NUMBEROFCHANNELS,CONTEXTS.ALLOWEDDEVHIGH,CONTEXTS.ALLOWEDDEVLOW from ATLAS_MUON_NSW_ELEC_QAQC.CONTEXTS left join ATLAS_MUON_NSW_ELEC_QAQC.ASPECTS ON ASPECTS.ASPECTNAME=CONTEXTS.ASPECTNAME left join ATLAS_MUON_NSW_ELEC_QAQC.COLLCOMPONENTS ON COLLCOMPONENTS.ASPECTNAME=ASPECTS.ASPECTNAME where COLLCOMPONENTS.COLLECTIONNAME like :1 and  CONTEXTS.SITENAME like :2"
                Type=Type[0:1]+tuple([self.Site])
            else:
                Command = "select CONTEXTS.CONTEXTNAME,ASPECTS.NUMBEROFCHANNELS,CONTEXTS.ALLOWEDDEVHIGH,CONTEXTS.ALLOWEDDEVLOW from ATLAS_MUON_NSW_ELEC_QAQC.CONTEXTS left join ATLAS_MUON_NSW_ELEC_QAQC.ASPECTS ON ASPECTS.ASPECTNAME=CONTEXTS.ASPECTNAME left join ATLAS_MUON_NSW_ELEC_QAQC.COLLCOMPONENTS ON COLLCOMPONENTS.ASPECTNAME=ASPECTS.ASPECTNAME where COLLCOMPONENTS.COLLECTIONNAME like :1"
                Type=Type[0:1]
                


            try:

            
                Results2 = self.RunShow(Command,Type,False)

                Line2=[]
                
                if len(Results2)<1: continue
                for Line in Results2:
                    Line2.append(Line[0])
                self.TableDict[Type[0]]=Line2
            except:
            
                print "Error occured while describing tables, Exiting Program"
                print "Command:", Command
                exit()

        
        return self.TableDict


    def GetVarType(self,NameTable):
        if not self.Check_Table(NameTable):
            print "Error!",NameTable, "is not found. Skipping..."
            return 0
        if NameTable in self.TypeDict.keys():
            return self.TypeDict[NameTable]
        
        Command = " SELECT ASPECTS.MEASTYPENAME,ASPECTS.NUMBEROFCHANNELS,CONTEXTS.ALLOWEDDEVHIGH,CONTEXTS.ALLOWEDDEVLOW,MEASTYPES.INPUTPATTERN FROM ATLAS_MUON_NSW_ELEC_QAQC.ASPECTS RIGHT JOIN ATLAS_MUON_NSW_ELEC_QAQC.CONTEXTS ON ASPECTS.ASPECTNAME=CONTEXTS.ASPECTNAME LEFT JOIN ATLAS_MUON_NSW_ELEC_QAQC.MEASTYPES ON ASPECTS.MEASTYPENAME=MEASTYPES.MEASTYPENAME WHERE CONTEXTS.CONTEXTNAME=:1"
        try:
            Results = self.RunShow(Command,tuple([NameTable]))
            Type = Results[0]    
            self.TypeDict[NameTable]=Type
            return Type
        except:
            print "Error! Type for",NameTable,"couldn't be retrieved."
            return
        
    def CheckInputs(self,NameTable,InputValue,InputChan=None):


        
        self.KnownVariables={"Volt":'float',"File":'file',"Status":"bool","Ratio":"float","ErrorCode":"string","ARRAY":"array","mVoltage":'float',"Gain":"float","OCR":"string"}

        Array=self.GetVarType(NameTable)
        if Array==0: return 0
        Type=self.GetVarType(NameTable)[0]
        NChan=self.GetVarType(NameTable)[1]
        High=self.GetVarType(NameTable)[2]
        Low=self.GetVarType(NameTable)[3]
        Pattern=self.GetVarType(NameTable)[4]
        


        if (InputChan or NChan) and not InputChan=="BYPASS":
            if InputChan==None:
                print "This measurment needs a channel specified, please use aItemNChan"
                return 0;
            if NChan=="":
                print "For this measurment channels are not allowed, please use aItem"
                return 0;
                
            elif int(InputChan)<0 or int(InputChan)>int(NChan):
                print "Channel out of range ( 0 -",NChan,")"
                return 0;
            else:
                return 1;
            


        if self.KnownVariables[Type] is 'file':
            return 2
        elif self.KnownVariables[Type] is 'array':
            return 3
        elif Pattern:
            CheckPat = re.compile(Pattern)
            if CheckPat.match(InputValue):
                if High and float(InputValue) > float(High):
                    print "Value higher then",High
                    return 0
                if Low and float(InputValue) < float(Low):
                    print "Valie lower then",Low
                    return 0
                
                return 1;
            else:
                print "Input pattern doesn't match ",Pattern
                return 0;
            

        else:
            print "Input pattern not defined"
            return 0



        # if not Type in self.KnownVariables.keys():
        #     print "Warning, Unkown Variable for",NameTable,"Skipping...\n Please contact an admin."
        #     return 0
        # if self.KnownVariables[Type] is 'float':
        #     if InputValue.replace('.','',1).isdigit() and InputValue.count('.')<2:
        #         return 1
        #     else:
        #         return 0
        # elif self.KnownVariables[Type] is 'int':
        #     if InputValue.isdigit():
        #         return 1
        #     else:
        #         return 0
        # elif self.KnownVariables[Type] is 'bool':
        #     if InputValue is "T" or InputValue is "F":
        #         return 1
        #     else:
        #         return 0



    def ChangeUser(self,UserName):
        self.user=UserName
        print "User set to",self.user





    def FindEQID(self,ID):

        try:
            if ID.isdigit():
                CMD="SELECT ID_EQUIPMENT FROM ATLAS_MUON_NSW_MM_LOG.EQUIPMENT WHERE ID_EQUIPMENT=:1 OR PARTSBATCHMTFID=:1 OR OTHERID=:1"            
            else:
                CMD="SELECT ID_EQUIPMENT FROM ATLAS_MUON_NSW_MM_LOG.EQUIPMENT WHERE PARTSBATCHMTFID=:1 OR OTHERID=:1"

            Results = self.RunShow(CMD,tuple([ID]))

            if Results and len(Results)==1 and len(Results[0])==1:
                return Results[0][0]
            else:
                return False
        except:
            return False







    def __del__(self):
        if not self.cnx is None:
            self.cnx.close()




    

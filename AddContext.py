from datetime import date, datetime, timedelta
import TDS_MySQL
import argparse
import sys
import readline
import os
import ROOT
import csv
import getpass

from openpyxl import *

def main(argv):


    readline.parse_and_bind('tab: complete')
    readline.parse_and_bind('set editing-mode vi')
    Tables={}
    try:
        MySQL = TDS_MySQL.TDS_MySQL("PlayGround")
        Tables=MySQL.Create_Table_Dict()


    except Exception as e:
        print e
        exit()


    parser=argparse.ArgumentParser()

    parser.add_argument("--q",dest="Quit",action="store_true",default=False,help="Exit Program")
    parser.add_argument("--File",dest="File", metavar="<File Name>",help="Reads and runs the commands from a file.")



    # Checks if there are input arguments directly comming while calling the program
    if len(argv)>0:
        sInput=argv
        refresh=False
    else:
        refresh=True # Don't touch this, needed for command seperation
        sInput=[] # Just Init


    while True:


        ## This Part Takes input ##
        if refresh:
            try:
                sInput = str(raw_input("\nPlease Enter what you would like to do. (-h for help): \n"))
                sInput=sInput.split()

            except:
                exit();
        

        ## Seperate the commands in the input to run them seperatly ##
        cInd=[]
        for i in range(0,len(sInput)):
            if "--" in sInput[i] and "-" is sInput[i][0] and "-" is sInput[i][1]:
                cInd.append(i)

        if len(cInd) > 1:
            tmpsInput = sInput[0:cInd[1]]
            sInput = sInput[cInd[1]:]
            refresh=False
        else:
            tmpsInput = sInput
            sInput=[]
            refresh=True

        
        #Part that runs the commands

        try:
            Commands=parser.parse_args(tmpsInput)
        except:
            print "Wrong Command Try again, -h for help, --q for quit"
            continue

        if len(tmpsInput) > 0:
            print "CMD:"," ".join(tmpsInput)


        if Commands.File:
            wb = load_workbook(Commands.File, data_only=True)
            ws = wb['Blatt1']


            print "Getting Collection"
            ColCol = 'I'
            Column = ws[ColCol]
            Collection = []
            for Cell in Column:
                
                if Cell.value is None: continue
                if str(Cell.value) == "COLLECTIONS": continue
                if str(Cell.value) == "COLLECTIONNAME": continue
                Desc = str(ws[chr(ord(ColCol)+1)+str(Cell.row)].value)
                Desc = Desc if Desc !="None" else ''
                Comment = str(ws[chr(ord(ColCol)+2)+str(Cell.row)].value)
                Comment = Comment if Comment !="None" else ''

                print Cell.value,Cell.column,Cell.row,Desc,Comment
                Collection.append([Cell.column,Cell.row,str(Cell.value),Desc,Comment])


            print "Getting Collcomponents"
            ColCol = 'L'
            Column = ws[ColCol]
            ColloComp = []
            for Cell in Column:
                if Cell.value is None: continue
                if str(Cell.value) == "COLLCOMPONENTS": continue
                if str(Cell.value) == "COLLECTIONNAME": continue
                Aspect = str(ws[chr(ord(ColCol)+1)+str(Cell.row)].value)
                Aspect = Aspect if Aspect !="None" else ''
                Comment = str(ws[chr(ord(ColCol)+2)+str(Cell.row)].value)
                Comment = Comment if Comment !="None" else ''
                print Cell.value,Cell.column,Cell.row,Aspect,Comment
                ColloComp.append([Cell.column,Cell.row,str(Cell.value),Aspect,Comment])


            print "Getting Aspects"
            ColCol = 'O'
            Column = ws[ColCol]
            Aspects = []
            for Cell in Column:
                if Cell.value is None: continue
                if str(Cell.value) == "ASPECTS": continue
                if str(Cell.value) == "ASPECTNAME": continue
                DESC = str(ws[chr(ord(ColCol)+1)+str(Cell.row)].value)
                DESC = DESC if DESC !="None" else ''
                EQTYP = str(ws[chr(ord(ColCol)+2)+str(Cell.row)].value)
                EQTYP = EQTYP if EQTYP !="None" else ''
                NUMOFCHAN = str(ws[chr(ord(ColCol)+3)+str(Cell.row)].value)
                NUMOFCHAN = NUMOFCHAN if NUMOFCHAN !="None" else ''
                MEASTYPE = str(ws[chr(ord(ColCol)+4)+str(Cell.row)].value)
                MEASTYPE = MEASTYPE if MEASTYPE !="None" else ''
                Comments = str(ws[chr(ord(ColCol)+5)+str(Cell.row)].value)
                Comments = Comments if Comments !="None" else ''
                print Cell.value,Cell.column,Cell.row,DESC,EQTYP,NUMOFCHAN,MEASTYPE,Comments
                Aspects.append([Cell.column,Cell.row,str(Cell.value),DESC,EQTYP,NUMOFCHAN,MEASTYPE,Comments])


            print "Getting Contexts"
            ColCol = 'U'
            Column = ws[ColCol]
            Context = []
            for Cell in Column:
                if Cell.value is None: continue
                if str(Cell.value) == "CONTEXTS": continue
                if str(Cell.value) == "CONTEXTNAME": continue
                DESC = str(ws[chr(ord(ColCol)+1)+str(Cell.row)].value)
                DESC = DESC if DESC !="None" else ''
                ASPECT = str(ws[chr(ord(ColCol)+2)+str(Cell.row)].value)
                ASPECT = ASPECT if ASPECT !="None" else ''                
                SITE = str(ws[chr(ord(ColCol)+3)+str(Cell.row)].value)
                SITE = SITE if SITE !="None" else ''
                DESIGNVAL = str(ws[chr(ord(ColCol)+4)+str(Cell.row)].value)
                DESIGNVAL = DESIGNVAL if DESIGNVAL !="None" else ''
                DevHigh = str(ws[chr(ord(ColCol)+5)+str(Cell.row)].value)
                DevHigh = DevHigh if DevHigh !="None" else ''
                DevLow = str(ws["AA"+str(Cell.row)].value)
                DevLow = DevLow if DevLow !="None" else ''
                Tool = str(ws["AB"+str(Cell.row)].value)
                Tool = Tool if Tool !="None" else ''
                Tolarance = str(ws["AC"+str(Cell.row)].value)
                Tolarance = Tolarance if Tolarance !="None" else ''
                Comments = str(ws["AD"+str(Cell.row)].value)
                Comments = Comments if Comments !="None" else ''


                print Cell.value,Cell.column,Cell.row,DESC,ASPECT,SITE,DESIGNVAL,DevHigh,DevLow,Tool,Tolarance,Comments
                Context.append([Cell.column,Cell.row,str(Cell.value),DESC,ASPECT,SITE,DESIGNVAL,DevHigh,DevLow,Tool,Tolarance,Comments])



            for item in Collection:
                if item[2] in Tables.keys(): continue
                Command = "INSERT INTO COLLECTIONS (COLLECTIONNAME,COLLECTIONDESC,COLLCOMMENT,USERCR,USERED) VALUES( :1, :2, :3, :4, :5)"
                user= getpass.getuser()
                InputValues=item[2:]+[user,user]
                print Command,InputValues
                MySQL.RunCommand(Command,tuple(InputValues))                    

            lAspect=[]
            for key in Tables.keys():
                for item in Tables[key]:
                    lAspect.append(item)

            for item in Aspects:
                if item[2] in lAspect: continue
                
                Command = "INSERT INTO ASPECTS (ASPECTNAME,ASPECTDESC,EQTYPEGROUPCODE,NUMBEROFCHANNELS,MEASTYPENAME,ASPCOMMENT,USERCR,USERED) VALUES( :1, :2, :3, :4, :5, :6, :7, :8)"
                user= getpass.getuser()
                InputValues=item[2:]+[user,user]
                print Command,InputValues
                MySQL.RunCommand(Command,tuple(InputValues))                    



            for item in ColloComp:

                if item[3] in lAspect: continue
                Command = "INSERT INTO COLLCOMPONENTS (COLLECTIONNAME,ASPECTNAME,COMPCOMMENT,USERCR,USERED) VALUES( :1, :2, :3, :4, :5)"
                user= getpass.getuser()
                InputValues=item[2:]+[user,user]
                print Command,InputValues
                MySQL.RunCommand(Command,tuple(InputValues))                    


            for item in Context:

                #if item[3] in lAspect: continue
                Command = "INSERT INTO Contexts (CONTEXTNAME,CONTEXTDESC,ASPECTNAME,SITENAME,DESIGNVALUE,ALLOWEDDEVHIGH,ALLOWEDDEVLOW,TOOL,MEASTOLERANCE,CONTEXTCOMMENT,USERCR,USERED) VALUES( :1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12)"
                user= getpass.getuser()
                InputValues=item[2:]+[user,user]
                print Command,InputValues
                MySQL.RunCommand(Command,tuple(InputValues))                    











            


        if Commands.Quit:
            del MySQL
            exit()


        
            
            

if __name__ == "__main__":
    main(sys.argv[1:])

from datetime import date, datetime, timedelta
import TDS_MySQL
import argparse
import sys
import readline
import os
import ROOT
import csv


def main(argv,MySQL=None,Loop=True):


    Site=False #Set it to false if you want to get all Sites
    Equipment=False #4 Digit Equipemnt Name, Set it False if you don't want it all
    
    readline.parse_and_bind('tab: complete')
    readline.parse_and_bind('set editing-mode vi')
    Tables={}

    try:

        if MySQL is None:
            MySQL = TDS_MySQL.TDS_MySQL("PlayGround",Site,Equipment)
            #MySQL = TDS_MySQL.TDS_MySQL("Production",Site,Equipment)
            Tables=MySQL.Create_Table_Dict()


    except Exception as e:
        print e
        exit()


    parser=argparse.ArgumentParser()

    parser.add_argument("--q",dest="Quit",action="store_true",default=False,help="Exit Program")
    parser.add_argument("--File",dest="File", metavar="<File Name>",help="Reads and runs the commands from a file.")
    parser.add_argument("--SetShifter",dest="Shifter", metavar="<ShifterName>",help="Changed the default shifter for this sessions")
    parser.add_argument("--FileCSV",dest="FileCSV", metavar="<CSV File Name>",help="Reads and save the CSV file.")
 
    parser.add_argument("--ls",dest="List",nargs='?',metavar="<The Item you want to learn more>",const=True,help="List the Tables")

    parser.add_argument("--aItem",dest="Add_Item",nargs=4,metavar=("<Test Name>","<Object ID>","<Measured Value>","<Is Valid (T/F)>"),help="Adds the items to given table;  Exp: --aItem VDD_A 2570 25 T")
    parser.add_argument("--aItemWTime",dest="Add_Item",nargs=5,metavar=("<Test Name>","<Object ID>","<Measured Value>","<Is Valid (T/F)>","<Time Of Measurment>"),help="Adds the items to given table;  Exp: --aItem VDD_A 2570 25 T 2018-01-01")


    parser.add_argument("--aItemNChan",dest="Add_Item_Chan",nargs=5,metavar=("<Test Name>","<Object ID>","<Measured Value>","<Is Valid (T/F)>","<Channel>"),help="Adds the items to given table;  Exp: --aItem VDD_A 2570 25 T 0 ")
    parser.add_argument("--aItemNChanWTime",dest="Add_Item_Chan",nargs=6,metavar=("<Test Name>","<Object ID>","<Measured Value>","<Is Valid (T/F)>","<Time Of Measurment>","<Channel>"),help="Adds the items to given table;  Exp: --aItem VDD_A 2570 25 T 2018-01-01 0")


    
    # parser.add_argument("--anTable",dest="Analyze_Item",nargs="*",metavar="<Table Name> <ColumnName>=<Input>...",help="Shows contents of a table with given attributes;  Exp: --sTable IIC_CONF Test_Date=2015-1-1 oFile=Out.txt")

    parser.add_argument("--sTable",dest="Show_Item",metavar="<Context Name>",help="Shows contents of a table with contex;  Exp: --sTable IIC_CONF")

    parser.add_argument("--sTableLog",dest="Show_Item_Log",nargs=2,help="Shows contents of a table with contex and write it to log file;  Exp: --sTable IIC_CONF Out.log")


    parser.add_argument("--DownloadFile",dest="DownloadFile",nargs=2,metavar="<Table Name>",help="Downloads the given file from Table given MEASSITEHAST;  Exp: --DownloadFile IIC_CONF UNKNOWN_1454")
    parser.add_argument("--sHistory",dest="Show_Hist",metavar="<TDS_ID>",help="Shows history of a given chip")



    # Checks if there are input arguments directly comming while calling the program
    if len(argv)>0:
        sInput=argv
        refresh=False
    else:
        refresh=True # Don't touch this, needed for command seperation
        sInput=[] # Just Init


    while True:


        ## This Part Takes input ##
        if refresh:
            try:
                sInput = str(raw_input("\nPlease Enter what you would like to do. (-h for help): \n"))
                sInput=sInput.split()

            except:
                exit();
        

        ## Seperate the commands in the input to run them seperatly ##
        cInd=[]
        for i in range(0,len(sInput)):
            if "--" in sInput[i] and "-" is sInput[i][0] and "-" is sInput[i][1]:
                cInd.append(i)

        if len(cInd) > 1:
            tmpsInput = sInput[0:cInd[1]]
            sInput = sInput[cInd[1]:]
            refresh=False
        else:
            tmpsInput = sInput
            sInput=[]
            refresh=True

        
        #Part that runs the commands

        try:
            Commands=parser.parse_args(tmpsInput)
        except:
            print "Wrong Command Try again, -h for help, --q for quit"
            continue

        if len(tmpsInput) > 0:
            print "CMD:"," ".join(tmpsInput)


        if Commands.File:
            File = open(Commands.File)
            # refresh=False;
            # sInput=File.read().replace("\n"," ").split()+sInput
            # print sInput
 
            for Line in File:
                if not Line[0:2]=="--": continue
                main(Line.split(),MySQL,False)

        if Commands.FileCSV:
            File = open(Commands.FileCSV)
            # spamreader = csv.reader(File)
            # refresh=False;
            # for row in spamreader:
            #     if row[0][0]=='#': continue
            #     row = [ item.strip() for item in row ]
            #     sInput=["--aItem"]+row+sInput

            spamreader = csv.reader(File)
            for row in spamreader:
                if row[0][0]=='#': continue
                row = [ item.strip() for item in row ]
                sInput=["--aItem"]+row+sInput
                main(sInput,MySQL,False)

        if Commands.List:
            if Commands.List is True:
                for TableName,Columns in Tables.iteritems():
                    print TableName,":","  ",
                    for Column in Columns:
                        print Column,
                    print
            else:
                if Commands.List in Tables.keys():
                    print Commands.List,":","  ",
                    for Column in Tables[Commands.List]:
                        print Column,
                    print
                elif MySQL.Check_Table(Commands.List):
                    for NameTable in Tables.keys():
                        if Commands.List in Tables[NameTable]:
                            print Commands.List,": COL=",NameTable,"; Var=",MySQL.GetVarType(Commands.List)

                else:
                    print "Nothing to display for", Commands.List


        if Commands.Add_Item:
            MySQL.AddItem(Commands.Add_Item[0],Commands.Add_Item[1:])
        if Commands.Add_Item_Chan:
            MySQL.AddItem(Commands.Add_Item_Chan[0],Commands.Add_Item_Chan[1:-1],Commands.Add_Item_Chan[-1])

            
        # if Commands.Mod_Item:
        #     MySQL.ModifyItem(Commands.Mod_Item[0],Commands.Mod_Item[1:])

        # if Commands.Del_Item:
        #     MySQL.DeleteItem(Commands.Del_Item[0:1])

        if Commands.Show_Item:
            MySQL.ShowTable(Commands.Show_Item)

        if Commands.Show_Item_Log:
            print Commands.Show_Item_Log
            try:
                Headers,Results = MySQL.ShowTable(Commands.Show_Item_Log[0],True)
                outFile = open(Commands.Show_Item_Log[1], 'w')

                row_format=[]
                line=""
                for Head in Headers:

                    # row_format.append("{:>"+str(len(Head))+"}")
                    # line+="-"*(len(Head)+9)
                    print>>outFile, Head,",",
                print
                print line
                for Line in Results:
                    for index in range(0,len(Headers)):
                        print>>outFile,str(Line[index]),",",
                    print>>outFile
                
                outFile.close()
                print "Output Written to",Commands.Show_Item_Log[1]

            except:
                print "Failed to write log file"
                




        if Commands.DownloadFile:
            MySQL.DownloadFile(Commands.DownloadFile[0],Commands.DownloadFile[1])

        if Commands.Show_Hist:
            MySQL.ShowHistory(Commands.Show_Hist)
            

        # if Commands.Analyze_Item:
        #     if Commands.Analyze_Item[0].split(".")[0] in Tables.keys():
        #         Columns,Results = MySQL.ShowItem(Commands.Analyze_Item[0],Commands.Analyze_Item[1:],True)

        #         if len(Results)>0:
        #             File = ROOT.TFile(Commands.Analyze_Item[0].split(".")[0]+".root","recreate")
        #             Hist = []
        #             Values=[]
        #             for index in range(0,len(Results[0])):
        #                 Values=[]
        #                 for Value in Results:
        #                     try:
        #                         if "ARRAY" in Columns[index]:
        #                             if Value[index] is None: continue;
        #                             Number = [ float(tmpNum) for tmpNum in Value[index].split(",")]
        #                         else:
        #                             Number=float(Value[index])
        #                     except:
        #                         continue
                            
        #                     if isinstance(Number, list):
        #                         for tmpNum in Number:
        #                             Values.append(tmpNum)
        #                     else:
        #                         Values.append(Number)
        #                 if len(Values)==0: continue
        #                 Max = max(Values)
        #                 Min = min (Values)
        #                 NValues=len(set(Values))
        #                 Hist.append(ROOT.TH1F(Columns[index],Columns[index],NValues,Min,Max+1))
        #                 for Value in Values:
        #                     Hist[-1].Fill(Value)
        #                 Hist[-1].Write()
        #                 print "Results are stored in",Commands.Analyze_Item[0].split(".")[0]+".root"
        #             File.Close()




        #     else:
        #         print "Table doesn't exist. Skipping..."


            

        if Commands.Shifter:
            user=Commands.Shifter

            MySQL.ChangeUser(user)

            

        if Commands.Quit:
            del MySQL
            exit()

        if not Loop:
            return
            
            

if __name__ == "__main__":
    main(sys.argv[1:])

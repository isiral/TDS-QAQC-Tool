import os
import sys
from ReadTDS import *
import datetime

FolderList =[]
ChipList={}
RejectedChipList={}

aResults=[]


for index in range(1,len(sys.argv)):
    FolderList.append(sys.argv[index])



for iFolderName in FolderList:
    ListOfFiles=os.listdir(iFolderName)


    for FileName in ListOfFiles:
        Prop,Results,PadChan = ReadTDSFile(iFolderName+"/"+FileName)
        try: 
            Chip = Prop["Chip Name:"].upper()

            if not Chip in ChipList.keys():
                if len(Chip)==11:
                    ChipList[Chip]=Prop
                else: 
                    RejectedChipList[Chip]=Prop

            if len(Chip)==11:
                aResults.append([Prop,Results,PadChan])

        except:
            print "Problem found for File",FileName


        


oFile = open("ListOfTDSChips.txt","w+")
print ChipList.keys()
ChipKeys = sorted(ChipList.keys(),key=str.lower)


print >> oFile, "OTHERID;EQTYPECODE;SUPPLIER;MANUFACTURINGDATE;MAJORLOCID;WEBSITEUSERCR;STATUSID"    

for Chip in ChipKeys:
   
    tStamp=ChipList[Chip]["Time:"].split("-")
    if tStamp[0]=="2018":
        tStamp="2017-01-01"
    elif tStamp[0]=="2017":
        tStamp="2016-01-01"
    else:
        tStamp="2019-01-01"

    OutItems=[Chip,"TDSA","Global Foundries",tStamp,"28","isiral","3"]

    print >> oFile,";".join(OutItems)
   

print 
print "Rejected Chips"
print " ".join(RejectedChipList.keys())



NameConv={}
NameConv["I2C:"]="TDS_I2C_STATUS_UM"
NameConv["PRBS:"]="TDS_PRBS_STATUS_UM"
NameConv["Router_Protocol:"]="TDS_ROUTER_TEST_STATUS_UM"
NameConv["Trigger:"]="TDS_TRIGGER_STATUS_UM"
NameConv["Bypass_Trigger:"]="TDS_BYPASS_TRIGGER_STATUS_UM"

oFile = open("TDSMeasurments","w+")
user=""


for Prop,Results,PadChan in aResults:


    if len(Results)<2:
        print "Incomplete results found. Skipping",Prop["File:"]
        continue


    Chip = Prop["Chip Name:"].upper()

    if not Prop["Tester:"].lower()==user:
        user=Prop["Tester:"].lower()
        print >> oFile, "--SetShifter",user



    for  Test in Results.keys():

        if "Pad_Mode:" in Test:
            continue


        if Results[Test]=="Success":
            Results[Test]="T"
        elif Results[Test]=="Failed":
            Results[Test]="F"
        elif Results[Test]=="Partial Failure":
            Results[Test]="F"
        else:
            print "Found Annamoly",Chip,Test,Results[Test]



 
        print >> oFile, "--aItemWTime",NameConv[Test],Chip,Results[Test],Results[Test],Prop["Time:"]

    for Chan in sorted(PadChan.keys(),key=str.lower):
        ChanRes = PadChan[Chan]
        if ChanRes=="passed!":
            ChanRes="T"
        elif ChanRes=="Failed!":
            ChanRes="F"
        else:
            print Chan,ChanRes,Prop["File:"]
            ChanRes="F"

        print >> oFile, "--aItemNChanWTime","TDS_PAD_MODE_STATUS_UM",Chip,ChanRes,ChanRes,Prop["Time:"],Chan





    print >> oFile, "--aItemWTime","TDS_TEST_LOG_UM",Chip,Prop["File:"],"T",Prop["Time:"]
    print >> oFile






oFile.close()

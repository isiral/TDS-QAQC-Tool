import sys


ADDMeas ={
"ADDC_Board_ADDC_ID":"ADDC_Board_ADDC_ID",
"ADDC_Board_SCA_ID":"ADDC_Board_SCA_ID",
"ADDC_Board_GBTx0_ID":"ADDC_Board_GBTx0_ID",
"ADDC_Board_GBTx1_ID":"ADDC_Board_GBTx1_ID",
"ADDC_Board_ART0_ID":"ADDC_Board_ART0_ID",
"ADDC_Board_ART1_ID":"ADDC_Board_ART1_ID",
"ADDC_Board_Stress_Screen":"ADDC_Board_Stress_Screen",
"ADDC_Board_Cooling_Plate":"ADDC_Board_Cooling_Plate",
"ADDC_Board_Visual_Comment":"ADDC_Board_Visual_Comment",
"ADDC_Func_PWR_1D5":"ADDC_Func_PWR_1D5",
"ADDC_Func_PWR_2D5":"ADDC_Func_PWR_2D5",
"ADDC_Func_PWR_Consume":"ADDC_Func_PWR_Consume",
"ADDC_Func_SCA_Comm":"ADDC_Func_SCA_Comm",
"ADDC_Func_ADC_Value":"ADDC_Func_ADC_Value",
"ADDC_Func_I2C_GBTx0":"ADDC_Func_I2C_GBTx0",
"ADDC_Func_I2C_GBTx1":"ADDC_Func_I2C_GBTx1",
"ADDC_Func_I2C_ART0":"ADDC_Func_I2C_ART0",
# "ADDC_Func_I2C_ART1":pass
# "ADDC_Func_ART_FrameAlign":pass
# "ADDC_Func_ART_Noise":pass
# "ADDC_Func_ART_Mask":pass
# "ADDC_Func_ART_BCR":pass
# "ADDC_Func_ART_Invert":pass
# "ADDC_Func_ART_Reverse":pass
# "ADDC_Func_ART_Deadtime":pass
# "ADDC_Func_ART_Hitmap":pass
# "ADDC_Func_ART_Hitadd":pass
}



def ReadADDC(InputName):

    MeasVal={}
    Outline=[]

    try:
        InputFile = open(InputName,"r")
    except:
        print "File",InputName,"Not Found or couldn't be oppened"
        return



    for line in InputFile:
        for Item in ADDMeas.keys():
            if Item in line:
                Value=line.replace(Item+" ","").replace("\n","").replace("\r","")
                Valid="T"
                if "pass" in Value:
                    Value = "T"
                elif "fail" in Value:
                    Value = "F"
                    Valid="T"
                elif "??" in Value or "-" in Value:
                    print "Value",Item,"Not Filled, continuing"
                    continue

                MeasVal[Item]=[Value,Valid]

                
    


    # Time for printing
    for Item in ADDMeas.keys():
        if Item is "ADDC_Board_ADDC_ID" or not Item in MeasVal.keys():
            continue

            

        Outline.append("--aItem "+ADDMeas[Item]+" "+MeasVal["ADDC_Board_ADDC_ID"][0]+" "+MeasVal[Item][0]+" "+MeasVal[Item][1])

    return Outline


if __name__ =="__main__":

    if not len(sys.argv)==2:
        print "File name not given"
        print "Please run the command as: $python ReadADDC.py addc_1000.log"
        exit()

    Outfile=open("ADDMeas","w+")
    #You can loop over this line with different log files
    Outline = ReadADDC(sys.argv[1])




    for line in Outline:
        print >> Outfile, line 

    Outfile.close()

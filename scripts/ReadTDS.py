import os
import sys

debug=False

def FindInLine(line,Search,splitby=" "):
    if Search in line:
        NoSpace=Search.replace(" ","")
        return line.replace(Search,NoSpace).split(splitby)[1].replace("\n","")
    else:
        return False



def ReadTDSFile(TxtFile=False):

    PropItems=["Chip Name:","Board Name:","Tester:","Time:"]
    Prop={}
    #We will need this information as well to save the log file
    Prop["File:"]=TxtFile
    ResultItems=["I2C:","PRBS:","Router_Protocol:","Trigger:","Bypass_Trigger:","Pad_Mode:"]
    Results={}

    
    PadChan={}
    TriggerChan=[]
    ChanMode=False


    with open(TxtFile) as file:
        for line in file:

            Test_Name=FindInLine(line,"Test Name:")
            #Set The oppertation mode to read channels or not
            if  Test_Name=="Pad_Mode":
                ChanMode="Pad"
                continue
            elif Test_Name=="Trigger":
                ChanMode="Trigger"
                continue
            #If find End Time, finnish channel mode
            elif FindInLine(line,"End Time:"):
                ChanMode=False
                continue

            
            if ChanMode=="Pad":
                Channel=FindInLine(line,"Channel")
                if not Channel or ":" in Channel:
                    continue

                PadChan[Channel]=FindInLine(line,"Channel "+Channel)



    
            #If not in channel mode read the file normally.
            if not ChanMode:
                for item in PropItems:
                    #Prevent ReWritting and saves cpu
                    if item in Prop.keys():
                        continue
                    tmp=FindInLine(line,item)
                    if not tmp is False:
                        Prop[item]=tmp
                        line=False #Makes it so that we continue with the next line
                        break
                #Makes it so that we continue with the next line if line is already used
                if not line:
                    continue
                #This is split because the results are not " " seperated
                for item in ResultItems:
                    #Prevent ReWritting and saves cpu
                    if item in Results.keys():
                        continue
                    tmp=FindInLine(line,item,":")
                    if not tmp is False:
                        Results[item]=tmp
                        line=False #Makes it so that we continue with the next line
                        break
                #Makes it so that we continue with the next line if line is already used
                if not line:
                    continue


 





    if debug:
        print "Properties:",Prop
        print "Results:",Results
        print "PadChan",PadChan
    
    return Prop,Results,PadChan










ReadTDSFile("TDSV2-00115_1.txt")

import os
import sys

#debug=True
ColumnName=["Chip_ID","TestDate","SerialNumber","TestNumber","TesterName","ReviewerName","ReviewDate","PassOrFail","RTGC_MTX1_UM","RTGC_MTX2_UM","RTGC_TP31_R_UM","RTGC_TP30_R_UM","RTGC_TP18_R_UM","RTGC_TP19_R_UM","RTGC_TP21_R_UM","RTGC_TP20_R_UM","RTGC_TP29_R_UM","RTGC_TP32_R_UM","RTGC_TP22_R_UM","RTGC_TP31_RRev_UM","RTGC_TP30_RRev_UM","RTGC_TP18_RRev_UM","RTGC_TP19_RRev_UM","RTGC_TP21_RRev_UM","RTGC_TP20_RRev_UM","RTGC_TP29_RRev_UM","RTGC_TP32_RRev_UM","RTGC_TP22_RRev_UM","RTGC_TP31_P_UM","RTGC_TP30_P_UM","RTGC_TP18_P_UM","RTGC_TP19_P_UM","RTGC_TP21_P_UM","RTGC_TP20_P_UM","RTGC_TP29_P_UM","RTGC_TP32_P_UM","RTGC_TP22_P_UM","RTGC_DS15_UM","RTGC_DS16_UM","RTGC_TP31_UM","RTGC_TP30_UM","RTGC_TP2_UM","RTGC_TP4_UM","RTGC_R284_UM","RTGC_R283_UM","RTGC_TP6_UM","RTGC_TP8_UM","RTGC_R286_UM","RTGC_R287_UM","RTGC_TP10_UM","RTGC_TP12_UM","RTGC_TP14_UM","RTGC_R288_UM","RTGC_R289_UM","RTGC_R290_UM","RTGC_FEAST_U6_UM","RTGC_FEAST_U5_UM","RTGC_FEAST_U8_UM","RTGC_FEAST_U4_UM","RTGC_FEAST_U7_UM","RTGC_FEAST_U10_UM","RTGC_FEAST_U9_UM","RTGC_FEAST_U11_UM","RTGC_TP18_DS22_V_UM","RTGC_TP19_DS21_V_UM","RTGC_TP21_DS19_V_UM","RTGC_TP20_DS20_V_UM","RTGC_TP29_DS17_V_UM","RTGC_TP32_DS18_V_UM","RTGC_TP22_DS23_V_UM","RTGC_TP18_DS22_VRev_UM","RTGC_TP19_DS21_VRev_UM","RTGC_TP21_DS19_VRev_UM","RTGC_TP20_DS20_VRev_UM","RTGC_TP29_DS17_VRev_UM","RTGC_TP32_DS23_VRev_UM","RTGC_TP22_DS23_VRev_UM","RTGC_TP18_DS22_P_UM","RTGC_TP19_DS21_P_UM","RTGC_TP21_DS19_P_UM","RTGC_TP20_DS20_P_UM","RTGC_TP29_DS17_P_UM","RTGC_TP32_DS23_P_UM","RTGC_TP22_DS23_P_UM","RTGC_DS2_UM","RTGC_DS1_UM","RTGC_J9_UM","RTGC_DS3_UM","RTGC_U16_UM","RTGC_J8_UM","RTGC_J7_UM","RTGC_RX_BER_UM_0","RTGC_RX_BER_UM_1","RTGC_RX_BER_UM_2","RTGC_RX_BER_UM_3","RTGC_RX_BER_UM_4","RTGC_RX_BER_UM_5","RTGC_RX_BER_UM_6","RTGC_RX_BER_UM_7","RTGC_RX_BER_UM_8","RTGC_RX_BER_UM_9","RTGC_RX_BER_UM_10","RTGC_RX_BER_UM_11","RTGC_TX_BER_UM_0","RTGC_TX_BER_UM_1","RTGC_TX_BER_UM_2","RTGC_TX_BER_UM_3","RTGC_RX_P_UM_0","RTGC_RX_P_UM_1","RTGC_RX_P_UM_2","RTGC_RX_P_UM_3","RTGC_RX_P_UM_4","RTGC_RX_P_UM_5","RTGC_RX_P_UM_6","RTGC_RX_P_UM_7","RTGC_RX_P_UM_8","RTGC_RX_P_UM_9","RTGC_RX_P_UM_10","RTGC_RX_P_UM_11","RTGC_TX_P_UM_0","RTGC_TX_P_UM_1","RTGC_TX_P_UM_2","RTGC_TX_P_UM_3","RTGC_EL2_UM","RTGC_U13_UM","RTGC_U14_UM","RTGC_X1_UM","RTGC_GTPRefClk_UM","RTGC_BCClk_UM","RTGC_U15Power_UM","RTGC_GPIO_UM","RTGC_RESET_B_UM","RTGC_SLVS_LVDS_UM","RTGC_PWR_DISEN_UM","RTGC_PassCondition_UM","RTGC_FinalFirmware6Copies_UM","RTGC_MassProductionGUI_UM","RTGC_GUI_UM","RTGC_BURN_UM"]
Type={}
Type["Chip_ID"]="X"
Type["TestDate"]="X"
Type["SerialNumber"]="X"
Type["TestNumber"]="X"
Type["TesterName"]="X"
Type["ReviewerName"]="X"
Type["ReviewDate"]="X"
Type["PassOrFail"]="X"
Type["RTGC_MTX1_UM"]="A"
Type["RTGC_MTX2_UM"]="A"
Type["RTGC_TP31_R_UM"]="B"
Type["RTGC_TP30_R_UM"]="B"
Type["RTGC_TP18_R_UM"]="B"
Type["RTGC_TP19_R_UM"]="B"
Type["RTGC_TP21_R_UM"]="B"
Type["RTGC_TP20_R_UM"]="B"
Type["RTGC_TP29_R_UM"]="B"
Type["RTGC_TP32_R_UM"]="B"
Type["RTGC_TP22_R_UM"]="B"
Type["RTGC_TP31_RRev_UM"]="B"
Type["RTGC_TP30_RRev_UM"]="B"
Type["RTGC_TP18_RRev_UM"]="B"
Type["RTGC_TP19_RRev_UM"]="B"
Type["RTGC_TP21_RRev_UM"]="B"
Type["RTGC_TP20_RRev_UM"]="B"
Type["RTGC_TP29_RRev_UM"]="B"
Type["RTGC_TP32_RRev_UM"]="B"
Type["RTGC_TP22_RRev_UM"]="B"
Type["RTGC_TP31_P_UM"]="Y"
Type["RTGC_TP30_P_UM"]="Y"
Type["RTGC_TP18_P_UM"]="Y"
Type["RTGC_TP19_P_UM"]="Y"
Type["RTGC_TP21_P_UM"]="Y"
Type["RTGC_TP20_P_UM"]="Y"
Type["RTGC_TP29_P_UM"]="Y"
Type["RTGC_TP32_P_UM"]="Y"
Type["RTGC_TP22_P_UM"]="Y"
Type["RTGC_DS15_UM"]="C"
Type["RTGC_DS16_UM"]="C"
Type["RTGC_TP31_UM"]="C"
Type["RTGC_TP30_UM"]="C"
Type["RTGC_TP2_UM"]="C"
Type["RTGC_TP4_UM"]="C"
Type["RTGC_R284_UM"]="C"
Type["RTGC_R283_UM"]="C"
Type["RTGC_TP6_UM"]="C"
Type["RTGC_TP8_UM"]="C"
Type["RTGC_R286_UM"]="C"
Type["RTGC_R287_UM"]="C"
Type["RTGC_TP10_UM"]="C"
Type["RTGC_TP12_UM"]="C"
Type["RTGC_TP14_UM"]="C"
Type["RTGC_R288_UM"]="C"
Type["RTGC_R289_UM"]="C"
Type["RTGC_R290_UM"]="C"
Type["RTGC_FEAST_U6_UM"]="C"
Type["RTGC_FEAST_U5_UM"]="C"
Type["RTGC_FEAST_U8_UM"]="C"
Type["RTGC_FEAST_U4_UM"]="C"
Type["RTGC_FEAST_U7_UM"]="C"
Type["RTGC_FEAST_U10_UM"]="C"
Type["RTGC_FEAST_U9_UM"]="C"
Type["RTGC_FEAST_U11_UM"]="C"
Type["RTGC_TP18_DS22_V_UM"]="B"
Type["RTGC_TP19_DS21_V_UM"]="B"
Type["RTGC_TP21_DS19_V_UM"]="B"
Type["RTGC_TP20_DS20_V_UM"]="B"
Type["RTGC_TP29_DS17_V_UM"]="B"
Type["RTGC_TP32_DS18_V_UM"]="B"
Type["RTGC_TP22_DS23_V_UM"]="B"
Type["RTGC_TP18_DS22_VRev_UM"]="B"
Type["RTGC_TP19_DS21_VRev_UM"]="B"
Type["RTGC_TP21_DS19_VRev_UM"]="B"
Type["RTGC_TP20_DS20_VRev_UM"]="B"
Type["RTGC_TP29_DS17_VRev_UM"]="B"
Type["RTGC_TP32_DS23_VRev_UM"]="B"
Type["RTGC_TP22_DS23_VRev_UM"]="B"
Type["RTGC_TP18_DS22_P_UM"]="Y"
Type["RTGC_TP19_DS21_P_UM"]="Y"
Type["RTGC_TP21_DS19_P_UM"]="Y"
Type["RTGC_TP20_DS20_P_UM"]="Y"
Type["RTGC_TP29_DS17_P_UM"]="Y"
Type["RTGC_TP32_DS23_P_UM"]="Y"
Type["RTGC_TP22_DS23_P_UM"]="Y"
Type["RTGC_DS2_UM"]="C"
Type["RTGC_DS1_UM"]="C"
Type["RTGC_J9_UM"]="C"
Type["RTGC_DS3_UM"]="C"
Type["RTGC_U16_UM"]="C"
Type["RTGC_J8_UM"]="C"
Type["RTGC_J7_UM"]="C"
Type["RTGC_RX_BER_UM_0"]="E"
Type["RTGC_RX_BER_UM_1"]="E"
Type["RTGC_RX_BER_UM_2"]="E"
Type["RTGC_RX_BER_UM_3"]="E"
Type["RTGC_RX_BER_UM_4"]="E"
Type["RTGC_RX_BER_UM_5"]="E"
Type["RTGC_RX_BER_UM_6"]="E"
Type["RTGC_RX_BER_UM_7"]="E"
Type["RTGC_RX_BER_UM_8"]="E"
Type["RTGC_RX_BER_UM_9"]="E"
Type["RTGC_RX_BER_UM_10"]="E"
Type["RTGC_RX_BER_UM_11"]="E"
Type["RTGC_TX_BER_UM_0"]="E"
Type["RTGC_TX_BER_UM_1"]="E"
Type["RTGC_TX_BER_UM_2"]="E"
Type["RTGC_TX_BER_UM_3"]="E"
Type["RTGC_RX_P_UM_0"]="Y"
Type["RTGC_RX_P_UM_1"]="Y"
Type["RTGC_RX_P_UM_2"]="Y"
Type["RTGC_RX_P_UM_3"]="Y"
Type["RTGC_RX_P_UM_4"]="Y"
Type["RTGC_RX_P_UM_5"]="Y"
Type["RTGC_RX_P_UM_6"]="Y"
Type["RTGC_RX_P_UM_7"]="Y"
Type["RTGC_RX_P_UM_8"]="Y"
Type["RTGC_RX_P_UM_9"]="Y"
Type["RTGC_RX_P_UM_10"]="Y"
Type["RTGC_RX_P_UM_11"]="Y"
Type["RTGC_TX_P_UM_0"]="Y"
Type["RTGC_TX_P_UM_1"]="Y"
Type["RTGC_TX_P_UM_2"]="Y"
Type["RTGC_TX_P_UM_3"]="Y"
Type["RTGC_EL2_UM"]="C"
Type["RTGC_U13_UM"]="C"
Type["RTGC_U14_UM"]="C"
Type["RTGC_X1_UM"]="C"
Type["RTGC_GTPRefClk_UM"]="C"
Type["RTGC_BCClk_UM"]="C"
Type["RTGC_U15Power_UM"]="C"
Type["RTGC_GPIO_UM"]="C"
Type["RTGC_RESET_B_UM"]="C"
Type["RTGC_SLVS_LVDS_UM"]="C"
Type["RTGC_PWR_DISEN_UM"]="C"
Type["RTGC_PassCondition_UM"]="Z"
Type["RTGC_FinalFirmware6Copies_UM"]="C"
Type["RTGC_MassProductionGUI_UM"]="C"
Type["RTGC_GUI_UM"]="C"
Type["RTGC_BURN_UM"]="C"


Method={}
Method["Chip_ID"]=0
Method["TestDate"]=0
Method["SerialNumber"]=0
Method["TestNumber"]=0
Method["TesterName"]=0
Method["ReviewerName"]=0
Method["ReviewDate"]=0
Method["PassOrFail"]=0
Method["RTGC_MTX1_UM"]=0
Method["RTGC_MTX2_UM"]=0
Method["RTGC_TP31_R_UM"]=28
Method["RTGC_TP30_R_UM"]=29
Method["RTGC_TP18_R_UM"]=30
Method["RTGC_TP19_R_UM"]=31
Method["RTGC_TP21_R_UM"]=32
Method["RTGC_TP20_R_UM"]=33
Method["RTGC_TP29_R_UM"]=34
Method["RTGC_TP32_R_UM"]=35
Method["RTGC_TP22_R_UM"]=36
Method["RTGC_TP31_RRev_UM"]=28
Method["RTGC_TP30_RRev_UM"]=29
Method["RTGC_TP18_RRev_UM"]=30
Method["RTGC_TP19_RRev_UM"]=31
Method["RTGC_TP21_RRev_UM"]=32
Method["RTGC_TP20_RRev_UM"]=33
Method["RTGC_TP29_RRev_UM"]=34
Method["RTGC_TP32_RRev_UM"]=35
Method["RTGC_TP22_RRev_UM"]=36
Method["RTGC_TP31_P_UM"]=0
Method["RTGC_TP30_P_UM"]=0
Method["RTGC_TP18_P_UM"]=0
Method["RTGC_TP19_P_UM"]=0
Method["RTGC_TP21_P_UM"]=0
Method["RTGC_TP20_P_UM"]=0
Method["RTGC_TP29_P_UM"]=0
Method["RTGC_TP32_P_UM"]=0
Method["RTGC_TP22_P_UM"]=0
Method["RTGC_DS15_UM"]=0
Method["RTGC_DS16_UM"]=0
Method["RTGC_TP31_UM"]=0
Method["RTGC_TP30_UM"]=0
Method["RTGC_TP2_UM"]=0
Method["RTGC_TP4_UM"]=0
Method["RTGC_R284_UM"]=0
Method["RTGC_R283_UM"]=0
Method["RTGC_TP6_UM"]=0
Method["RTGC_TP8_UM"]=0
Method["RTGC_R286_UM"]=0
Method["RTGC_R287_UM"]=0
Method["RTGC_TP10_UM"]=0
Method["RTGC_TP12_UM"]=0
Method["RTGC_TP14_UM"]=0
Method["RTGC_R288_UM"]=0
Method["RTGC_R289_UM"]=0
Method["RTGC_R290_UM"]=0
Method["RTGC_FEAST_U6_UM"]=0
Method["RTGC_FEAST_U5_UM"]=0
Method["RTGC_FEAST_U8_UM"]=0
Method["RTGC_FEAST_U4_UM"]=0
Method["RTGC_FEAST_U7_UM"]=0
Method["RTGC_FEAST_U10_UM"]=0
Method["RTGC_FEAST_U9_UM"]=0
Method["RTGC_FEAST_U11_UM"]=0
Method["RTGC_TP18_DS22_V_UM"]=77
Method["RTGC_TP19_DS21_V_UM"]=78
Method["RTGC_TP21_DS19_V_UM"]=79
Method["RTGC_TP20_DS20_V_UM"]=80
Method["RTGC_TP29_DS17_V_UM"]=81
Method["RTGC_TP32_DS18_V_UM"]=82
Method["RTGC_TP22_DS23_V_UM"]=83
Method["RTGC_TP18_DS22_VRev_UM"]=77
Method["RTGC_TP19_DS21_VRev_UM"]=78
Method["RTGC_TP21_DS19_VRev_UM"]=79
Method["RTGC_TP20_DS20_VRev_UM"]=80
Method["RTGC_TP29_DS17_VRev_UM"]=81
Method["RTGC_TP32_DS23_VRev_UM"]=82
Method["RTGC_TP22_DS23_VRev_UM"]=83
Method["RTGC_TP18_DS22_P_UM"]=0
Method["RTGC_TP19_DS21_P_UM"]=0
Method["RTGC_TP21_DS19_P_UM"]=0
Method["RTGC_TP20_DS20_P_UM"]=0
Method["RTGC_TP29_DS17_P_UM"]=0
Method["RTGC_TP32_DS23_P_UM"]=0
Method["RTGC_TP22_DS23_P_UM"]=0
Method["RTGC_DS2_UM"]=0
Method["RTGC_DS1_UM"]=0
Method["RTGC_J9_UM"]=0
Method["RTGC_DS3_UM"]=0
Method["RTGC_U16_UM"]=0
Method["RTGC_J8_UM"]=0
Method["RTGC_J7_UM"]=0
Method["RTGC_RX_BER_UM_0"]=107
Method["RTGC_RX_BER_UM_1"]=108
Method["RTGC_RX_BER_UM_2"]=109
Method["RTGC_RX_BER_UM_3"]=110
Method["RTGC_RX_BER_UM_4"]=111
Method["RTGC_RX_BER_UM_5"]=112
Method["RTGC_RX_BER_UM_6"]=113
Method["RTGC_RX_BER_UM_7"]=114
Method["RTGC_RX_BER_UM_8"]=115
Method["RTGC_RX_BER_UM_9"]=116
Method["RTGC_RX_BER_UM_10"]=117
Method["RTGC_RX_BER_UM_11"]=118
Method["RTGC_TX_BER_UM_0"]=119
Method["RTGC_TX_BER_UM_1"]=120
Method["RTGC_TX_BER_UM_2"]=121
Method["RTGC_TX_BER_UM_3"]=122
Method["RTGC_RX_P_UM_0"]=0
Method["RTGC_RX_P_UM_1"]=1
Method["RTGC_RX_P_UM_2"]=2
Method["RTGC_RX_P_UM_3"]=3
Method["RTGC_RX_P_UM_4"]=4
Method["RTGC_RX_P_UM_5"]=5
Method["RTGC_RX_P_UM_6"]=6
Method["RTGC_RX_P_UM_7"]=7
Method["RTGC_RX_P_UM_8"]=8
Method["RTGC_RX_P_UM_9"]=9
Method["RTGC_RX_P_UM_10"]=10
Method["RTGC_RX_P_UM_11"]=11
Method["RTGC_TX_P_UM_0"]=0
Method["RTGC_TX_P_UM_1"]=1
Method["RTGC_TX_P_UM_2"]=2
Method["RTGC_TX_P_UM_3"]=3
Method["RTGC_EL2_UM"]=0
Method["RTGC_U13_UM"]=0
Method["RTGC_U14_UM"]=0
Method["RTGC_X1_UM"]=0
Method["RTGC_GTPRefClk_UM"]=0
Method["RTGC_BCClk_UM"]=0
Method["RTGC_U15Power_UM"]=0
Method["RTGC_GPIO_UM"]=0
Method["RTGC_RESET_B_UM"]=0
Method["RTGC_SLVS_LVDS_UM"]=0
Method["RTGC_PWR_DISEN_UM"]=0
Method["RTGC_PassCondition_UM"]=0
Method["RTGC_FinalFirmware6Copies_UM"]=0
Method["RTGC_MassProductionGUI_UM"]=0
Method["RTGC_GUI_UM"]=0
Method["RTGC_BURN_UM"]=0
oFile = open("RTGCMeasurement.txt","w+")

fFile = open("ListofRTGCChips.txt","w+")
print >> fFile, "OTHERID;EQTYPECODE;SUPPLIER;MANUFACTURINGDATE;MAJORLOCID;WEBSITEUSERCR;STATUSID"

def SetShifter(Name):
    print >> oFile, "--SetShifter",Name

def aItemWTime(Mea,ChipN,Value,TF,time):
    print >> oFile, "--aItemWTime",Mea,ChipN,Value,TF,time
def aItemNChanWTime (Mea,ChipN,Value,TF,time,channel):
    print >> oFile, "--aItemNChanWTime",Mea,ChipN,Value,TF,time,channel

def AType(DictA={},indexA=0):
    SetShifter(DictA["TesterName"])
    if "XXX" in DictA[ColumnName[indexA]]:
    	aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]],"F",DictA["TestDate"])    
    else:
        aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]],"T",DictA["TestDate"])

def BType(DictA={},indexA=0):
    if "Rev" in ColumnName[indexA] and  DictA["ReviewerName"]!="":
        SetShifter(DictA["ReviewerName"])
        aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]],DictA[ColumnName[Method[ColumnName[indexA]]]][0:1].upper(),DictA["ReviewDate"])
    else:
        SetShifter(DictA["TesterName"])        
        aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]],DictA[ColumnName[Method[ColumnName[indexA]]]][0:1].upper(),DictA["TestDate"]) 

def CType(DictA={},indexA=0):
    SetShifter(DictA["TesterName"])
    aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]][0:1].upper(),DictA[ColumnName[indexA]][0:1].upper(),DictA["TestDate"])

def EType(DictA={},indexA=0,chan=""):
    SetShifter(DictA["TesterName"])
    aItemNChanWTime(ColumnName[indexA][:-len(chan)-1],DictA["Chip_ID"],DictA[ColumnName[indexA]],DictA[ColumnName[Method[ColumnName[indexA]]]][0:1].upper(),DictA["TestDate"],chan)

def ZType(DictA={},indexA=0):
    SetShifter(DictA["TesterName"])
    if "0" in DictA[ColumnName[indexA]]:
        aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]],"F",DictA["TestDate"])
    else:
        aItemWTime(ColumnName[indexA],DictA["Chip_ID"],DictA[ColumnName[indexA]],"T",DictA["TestDate"])


def ReadRTGC(CSVFile=False):
    debug=True
    with open(CSVFile) as file:
        Nline=0
        for line in file:
            if Nline==0:
                Nline=1
            else:
                debug=False
              #  continue
             
            Measurement=line.split(",")
            #print Measurement
       	    Dict={}
          
            for index in range(0,len(ColumnName)):          
		 Dict[ColumnName[index]]=Measurement[index]
            #print Dict 
            Dict["Chip_ID"]=Measurement[0][:-2]
            Dict["TestDate"]=Measurement[1].replace("/","-")
            Dict["ReviewDate"]=Dict["ReviewDate"].replace("/","-")
            Dict["TesterName"]=Dict["TesterName"].replace(" ","_")
            Dict["ReviewerName"]=Dict["ReviewerName"].replace(" ","_")
            #print Dict
	    EQID=Measurement[0][:-2]
            EQIDValid=Measurement[0][-1:]
            if "0" in EQIDValid:
                EQLGID="Not-Known-Yet"
                EQBuildDate="XXXX-XX-XX" #temp value
                OutItems=[EQLGID,EQID,"RTGC","University Michigan",EQBuildDate,"28","zhongyuk","3"]
                print >>fFile,";".join(OutItems)
            for index in range(0,len(ColumnName)):
		if (debug==True):
                    print ColumnName[index],Dict[ColumnName[index]]

                if Type[ColumnName[index]]=="X":
                    continue
                elif  Type[ColumnName[index]]=="A":
                    AType(Dict,index)
                elif  Type[ColumnName[index]]=="B":
                    BType(Dict,index)           
                elif  Type[ColumnName[index]]=="C":
                    CType(Dict,index)
                elif  Type[ColumnName[index]]=="E":
                    chan=ColumnName[index].split("_")[4]
                    EType(Dict,index,chan)
                elif Type[ColumnName[index]]=="Z":
                    ZType(Dict,index)
                 
"""
            TMeasurementA=Measurement[8:8+2] 
            #print TMeasurementA
            TResultA=[]
            for TMA in TMeasurementA:
                if "XXX" in TMA:
                    TResultA.append("F")
                else:
                    TResultA.append("T")
            #print TResultA

            TMeasurementB=Measurement[10:19]
            #print TMeasurementB
            TReB=Measurement[28:37]
            TResultB=[]
            for TMA in TReB:
                if "False" in TMA:
                    TResultB.append("F")
                else:
                    TResultB.append("T")
            #print TResultB
            
 
            TMeasurementC=Measurement[19:28]
            #print TMeasurementC
            TReC=Measurement[28:37]
            TResultC=[]
            for TMA in TReC:
                if "False" in TMA:
                    TResultC.append("F")
                else:
                    TResultC.append("T")
            #print TResultC

            TMeasurementD=Measurement[37:63]
            #print TMeasurementD
            TReD=Measurement[37:63]
            TResultD=[]
            for TMA in TReD:
                if "false" in TMA.lower():
                    TResultD.append("F")
                elif "true" in TMA.lower():
                    TResultD.append("T")
                else:
                    print "ilegal input phase D"
            #print TResultD
"""


ReadRTGC("RTGCtest.csv")
